#!/bin/bash

# https://gist.github.com/olasd/9841772
# Diffusion youtube avec ffmpeg

# Configurer youtube avec une résolution 720p. La vidéo n'est pas scalée.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CONF_FILE=${DIR}/youtube.conf

source ${CONF_FILE}.defaults

if [ -f ${CONF_FILE} ]; then
	source ${CONF_FILE}
fi

ffmpeg \
    -i "$SOURCE" -deinterlace \
    -f alsa -ac 1 -i hw:${AUDIO_SRC} \
    -vcodec libx264 -pix_fmt yuv420p -preset $QUALITY -r $FPS -g $(($FPS * 2)) -b:v $VBR -profile:v baseline -tune zerolatency \
    -acodec libmp3lame -ac 1 -ar 44100 -threads 6 -qscale 3 -b:a 712000 -bufsize 512k \
    -loglevel ${LOG_LEVEL} -hide_banner \
    -f flv "$URL/$KEY"
